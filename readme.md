# Chatbot using Flask

## How to run backend:
This repo currently contains the starter files.

Clone repo and create a virtual environment
```
$ git clone https://github.com/prve17/chatbot.git
$ cd chatbot-deployment
$ python3 -m venv venv
$ .\venv\Scripts\activate
```
Install dependencies
```
$ (venv) pip install Flask torch torchvision nltk
```
Install nltk package
```
$ (venv) python
>>> import nltk
>>> nltk.download('punkt')
```

Run
```
$ (venv) python train.py
$ (venv) python chat.py
```

## How to run frontend:

Run
```
$ (venv) python app.py
```

Then open the IP address you get after running python app.py